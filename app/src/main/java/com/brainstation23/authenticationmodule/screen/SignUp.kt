package com.brainstation23.authenticationmodule.screen


import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material.icons.filled.Person
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.brainstation23.authenticationmodule.R
import com.brainstation23.authenticationmodule.Routes
import com.brainstation23.authenticationmodule.components.FilledButton
import com.brainstation23.authenticationmodule.ui.theme.Purple700
import com.brainstation23.authenticationmodule.viewModel.SignUpViewModel

@Composable
fun SignUpPage(
    navController: NavHostController,
    vm: SignUpViewModel,
) {
    if (vm.state.response != null) {
        navController.navigate(Routes.Verification.route.replace(
            "{email}",
            vm.email
        ))
    }

    if (vm.state.message.isNotEmpty()) {
        Toast.makeText(
            navController.context,
            vm.state.message,
            Toast.LENGTH_SHORT
        ).show()
    }

    Box(modifier = Modifier.background(Color.White))
    {
        Column(
            modifier = Modifier
                .padding(20.dp)
                .fillMaxSize()
                .background(Color.White),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            val firstName = remember { mutableStateOf(TextFieldValue()) }
            val lastName = remember { mutableStateOf(TextFieldValue()) }
            val email = remember { mutableStateOf(TextFieldValue()) }
            val password = remember { mutableStateOf(TextFieldValue()) }

            LazyColumn(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                item {

                    Image(
                        modifier = Modifier.size(150.dp),
                        painter = painterResource(id = R.drawable.logo),
                        contentDescription = null,
                    )

                    Spacer(modifier = Modifier.height(20.dp))
                    OutlinedTextField(
                        modifier = Modifier.fillMaxWidth(),
                        label = { Text(text = "First Name") },
                        placeholder = { Text(text = "Enter your first name") },
                        leadingIcon = {
                            Icon(
                                imageVector = Icons.Default.Person,
                                contentDescription = "Name"
                            )
                        },
                        value = firstName.value,
                        onValueChange = { firstName.value = it },
                    )

                    Spacer(modifier = Modifier.height(20.dp))
                    OutlinedTextField(
                        modifier = Modifier.fillMaxWidth(),
                        label = { Text(text = "Last Name") },
                        placeholder = { Text(text = "Enter your last name") },
                        leadingIcon = {
                            Icon(
                                imageVector = Icons.Default.Person,
                                contentDescription = "Name"
                            )
                        },
                        value = lastName.value,
                        onValueChange = { lastName.value = it },
                    )

                    Spacer(modifier = Modifier.height(20.dp))
                    OutlinedTextField(
                        modifier = Modifier.fillMaxWidth(),
                        label = { Text(text = "Email") },
                        placeholder = { Text(text = "Enter your email") },
                        leadingIcon = {
                            Icon(
                                imageVector = Icons.Default.Email,
                                contentDescription = "Email Icon"
                            )
                        },
                        value = email.value,
                        onValueChange = { email.value = it },
                    )

                    Spacer(modifier = Modifier.height(20.dp))
                    OutlinedTextField(
                        modifier = Modifier.fillMaxWidth(),
                        label = { Text(text = "Password") },
                        placeholder = { Text(text = "Enter your password") },
                        leadingIcon = {
                            Icon(
                                imageVector = Icons.Default.Lock,
                                contentDescription = "Email Icon"
                            )
                        },
                        visualTransformation = PasswordVisualTransformation(),
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
                        value = password.value,
                        onValueChange = { password.value = it },
                    )

                    Spacer(modifier = Modifier.height(40.dp))
                    FilledButton(
                        text = "Sign Up",
                        isLoading = vm.state.isLoading,
                        onClick = {
                            vm.signUp(
                                firstName = firstName.value.text,
                                lastName = lastName.value.text,
                                email = email.value.text,
                                password = password.value.text
                            )
                        },
                    )

                    Spacer(modifier = Modifier.height(20.dp))
                    ClickableText(
                        text = AnnotatedString("Already have an account? Click here"),
                        modifier = Modifier.padding(20.dp),
                        onClick = {
                            navController.popBackStack()
                        },
                        style = TextStyle(
                            fontSize = 14.sp,
                            fontFamily = FontFamily.Default,
                            textDecoration = TextDecoration.Underline,
                            color = Purple700
                        )
                    )
                }
            }

        }
    }
}