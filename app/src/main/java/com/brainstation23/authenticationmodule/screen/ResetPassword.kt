package com.brainstation23.authenticationmodule.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Lock
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.brainstation23.authenticationmodule.R
import com.brainstation23.authenticationmodule.Routes
import com.brainstation23.authenticationmodule.components.FilledButton

@Composable
fun ResetPasswordPage(navController: NavController) {
    Box(
        modifier = Modifier.background(Color.White)
    )
    {
        Column(
            modifier = Modifier
                .padding(20.dp)
                .background(Color.White),
        ) {
            Box(
                contentAlignment = Alignment.TopStart
            ) {
                Icon(
                    modifier = Modifier
                        .size(30.dp)
                        .clickable {
                            navController.popBackStack()
                        },
                    imageVector = Icons.Default.ArrowBack,
                    contentDescription = null
                )
            }
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.White),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                val newPassword = remember { mutableStateOf(TextFieldValue()) }
                val confirmNewPassword = remember { mutableStateOf(TextFieldValue()) }

                Image(
                    modifier = Modifier.size(150.dp),
                    painter = painterResource(id = R.drawable.logo),
                    contentDescription = null
                )

                Spacer(modifier = Modifier.height(20.dp))
                OutlinedTextField(
                    modifier = Modifier.fillMaxWidth(),
                    label = { Text(text = "New Password") },
                    placeholder = { Text(text = "Enter your password") },
                    leadingIcon = {
                        Icon(
                            imageVector = Icons.Default.Lock,
                            contentDescription = "Lock Icon"
                        )
                    },
                    value = newPassword.value,
                    onValueChange = { newPassword.value = it }
                )


                Spacer(modifier = Modifier.height(20.dp))
                OutlinedTextField(
                    modifier = Modifier.fillMaxWidth(),
                    label = { Text(text = "Confirm New Password") },
                    placeholder = { Text(text = "Enter your password") },
                    leadingIcon = {
                        Icon(
                            imageVector = Icons.Default.Lock,
                            contentDescription = "Lock Icon"
                        )
                    },
                    value = confirmNewPassword.value,
                    onValueChange = { confirmNewPassword.value = it }
                )

                Spacer(modifier = Modifier.height(40.dp))
                FilledButton(
                    text = "Change Password",
                    onClick = {
                        navController.navigate(Routes.Login.route) {
                            popUpTo(Routes.ResetPassword.route) {
                                inclusive = true
                            }
                        }
                    },
                )
            }
        }
    }
}