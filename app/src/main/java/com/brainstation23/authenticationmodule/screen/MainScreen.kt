package com.brainstation23.authenticationmodule.screen

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.brainstation23.authenticationmodule.Routes
import com.brainstation23.authenticationmodule.viewModel.LoginViewModel
import com.brainstation23.authenticationmodule.viewModel.SignUpViewModel
import com.brainstation23.authenticationmodule.viewModel.VerificationViewModel

@Composable
fun MainScreen() {
    val navController = rememberNavController()
    val signUpViewModel = SignUpViewModel()
    val verificationViewModel = VerificationViewModel()
    val loginViewModel = LoginViewModel()

    NavHost(
        navController = navController,
        startDestination = Routes.Login.route
    ) {
        composable(Routes.SignUp.route) {
            SignUpPage(
                navController = navController,
                vm = signUpViewModel,
            )
        }

        composable(Routes.Login.route) {
            LoginPage(
                navController = navController,
                vm = loginViewModel,
            )
        }

        composable(Routes.ForgotPassword.route) {
            ForgotPasswordPage(
                navController = navController
            )
        }

        composable(Routes.Verification.route) {

            val email: String? = it.arguments?.getString("email")

            VerificationPage(
                navController = navController,
                vm = verificationViewModel,
                email = email
            )
        }

        composable(Routes.ResetPassword.route) {
            ResetPasswordPage(
                navController = navController
            )
        }

        composable(Routes.Home.route) {
            HomePage(
                navController = navController
            )
        }

    }
}