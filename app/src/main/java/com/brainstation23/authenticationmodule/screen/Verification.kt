package com.brainstation23.authenticationmodule.screen

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Lock
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.brainstation23.authenticationmodule.R
import com.brainstation23.authenticationmodule.Routes
import com.brainstation23.authenticationmodule.components.FilledButton
import com.brainstation23.authenticationmodule.viewModel.VerificationViewModel

@Composable
fun VerificationPage(
    navController: NavController,
    vm: VerificationViewModel,
    email: String? = null,
) {

    if (vm.state.response != null) {
        navigate(navController)
    }

    if (vm.state.message.isNotEmpty()) {
        Toast.makeText(
            navController.context,
            vm.state.message,
            Toast.LENGTH_SHORT
        ).show()
    }

    Box(
        modifier = Modifier.background(Color.White)
    )
    {
        Column(
            modifier = Modifier
                .padding(20.dp)
                .background(Color.White),
        ) {
            Box(
                contentAlignment = Alignment.TopStart
            ) {
                Icon(
                    modifier = Modifier
                        .size(30.dp)
                        .clickable {
                            navController.popBackStack()
                        },
                    imageVector = Icons.Default.ArrowBack,
                    contentDescription = null
                )
            }
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.White),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                val otp = remember { mutableStateOf(TextFieldValue()) }
                Image(
                    modifier = Modifier.size(150.dp),
                    painter = painterResource(id = R.drawable.logo),
                    contentDescription = null
                )

                Spacer(modifier = Modifier.height(20.dp))
                OutlinedTextField(
                    modifier = Modifier.fillMaxWidth(),
                    label = { Text(text = "OTP") },
                    placeholder = { Text(text = "Enter your OTP") },
                    leadingIcon = {
                        Icon(
                            imageVector = Icons.Default.Lock,
                            contentDescription = "Lock Icon"
                        )
                    },
                    value = otp.value,
                    onValueChange = { otp.value = it }
                )

                Spacer(modifier = Modifier.height(40.dp))
                FilledButton(
                    text = "Verify Email",
                    isLoading = vm.state.isLoading,
                    onClick = {
                        vm.verify(
                            email = email!!,
                            otp = otp.value.text
                        )
                    },
                )
            }
        }
    }
}

private fun navigate(navController: NavController) {
    val previousRoute: String? =
        navController.previousBackStackEntry?.destination?.route

    if (previousRoute == Routes.SignUp.route) {
        navController.navigate(Routes.Login.route) {
            popUpTo(Routes.Login.route) {
                inclusive = true
            }
        }

    } else {
        navController.navigate(Routes.ResetPassword.route) {
            popUpTo(Routes.Verification.route) {
                inclusive = true
            }
        }
    }
}