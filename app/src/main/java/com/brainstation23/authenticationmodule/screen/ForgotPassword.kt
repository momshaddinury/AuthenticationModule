package com.brainstation23.authenticationmodule.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Email
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.brainstation23.authenticationmodule.R
import com.brainstation23.authenticationmodule.Routes
import com.brainstation23.authenticationmodule.components.FilledButton

@Composable
fun ForgotPasswordPage(navController: NavController) {
    Box(
        modifier = Modifier.background(Color.White)
    )
    {
        Column(
            modifier = Modifier
                .padding(20.dp)
                .background(Color.White),
        ) {
            Box(
                contentAlignment = Alignment.TopStart
            ) {
                Icon(
                    modifier = Modifier
                        .size(30.dp)
                        .clickable {
                            navController.popBackStack()
                        },
                    imageVector = Icons.Default.ArrowBack,
                    contentDescription = null
                )
            }
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.White),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                val email = remember { mutableStateOf(TextFieldValue()) }
                Image(
                    modifier = Modifier.size(150.dp),
                    painter = painterResource(id = R.drawable.logo),
                    contentDescription = null
                )

                Spacer(modifier = Modifier.height(20.dp))
                OutlinedTextField(
                    modifier = Modifier.fillMaxWidth(),
                    label = { Text(text = "Email") },
                    placeholder = { Text(text = "Enter your email") },
                    leadingIcon = {
                        Icon(
                            imageVector = Icons.Default.Email,
                            contentDescription = "Email Icon"
                        )
                    },
                    value = email.value,
                    onValueChange = { email.value = it }
                )

                Spacer(modifier = Modifier.height(40.dp))
                FilledButton(
                    text = "Send OTP",
                    onClick = {
                        navController.navigate(Routes.Verification.route) {
                            popUpTo(Routes.ForgotPassword.route) {
                                inclusive = true
                            }
                        }
                    },
                )
            }
        }
    }
}