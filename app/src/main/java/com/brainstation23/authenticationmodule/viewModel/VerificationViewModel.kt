package com.brainstation23.authenticationmodule.viewModel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.brainstation23.authenticationmodule.data.VerificationRequest
import com.brainstation23.authenticationmodule.data.VerificationResponse
import com.brainstation23.authenticationmodule.service.APIService
import kotlinx.coroutines.launch

class VerificationViewModel : ViewModel() {
    var state: VerificationState by mutableStateOf(VerificationState())

    private val apiService = APIService.getInstance()

    fun verify(
        email: String,
        otp: String
    ) {
        viewModelScope.launch {
            try {
                state = state.copyWith(isLoading = true)

                VerificationRequest(
                    email = email,
                    otp = otp
                ).also {
                    state = state.copyWith(response = apiService.verifyOtp(it))
                }
            } catch (e: Exception) {
                e.printStackTrace()
                state = state.copyWith(isLoading = false, message = e.message.toString())
            } finally {
                state = state.copyWith(isLoading = false)
            }
        }
    }
}

class VerificationState {
    var isLoading: Boolean by mutableStateOf(false)
    var response: VerificationResponse? by mutableStateOf<VerificationResponse?>(null)
    var message: String by mutableStateOf("")

    fun copyWith(
        isLoading: Boolean = this.isLoading,
        response: VerificationResponse? = this.response,
        message: String = this.message
    ): VerificationState {
        return VerificationState().also {
            it.isLoading = isLoading
            it.response = response
            it.message = message
        }
    }
}