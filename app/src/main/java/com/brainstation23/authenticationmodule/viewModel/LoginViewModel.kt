package com.brainstation23.authenticationmodule.viewModel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.brainstation23.authenticationmodule.data.LoginRequest
import com.brainstation23.authenticationmodule.data.LoginResponse
import com.brainstation23.authenticationmodule.service.APIService
import kotlinx.coroutines.launch

class LoginViewModel : ViewModel() {
    var state: LoginState by mutableStateOf(LoginState())

    private val apiService = APIService.getInstance()

    fun login(
        email: String,
        password: String,
    ) {
        viewModelScope.launch {
            try {
                state = state.copyWith(isLoading = true)

                LoginRequest(
                    email = email,
                    password = password,
                    os = "Android",
                    model = "Android",
                    fcmToken = "Android"
                ).also {
                    state = state.copyWith(response = apiService.login(it))
                }
            } catch (e: Exception) {
                e.printStackTrace()
                state = state.copyWith(isLoading = false, message = e.message.toString())
            } finally {
                state = state.copyWith(isLoading = false)
            }
        }
    }
}

class LoginState {
    var isLoading: Boolean by mutableStateOf(false)
    var response: LoginResponse? by mutableStateOf<LoginResponse?>(null)
    var message: String by mutableStateOf("")

    fun copyWith(
        isLoading: Boolean = this.isLoading,
        response: LoginResponse? = this.response,
        message: String = this.message
    ): LoginState {
        return LoginState().also {
            it.isLoading = isLoading
            it.response = response
            it.message = message
        }
    }
}