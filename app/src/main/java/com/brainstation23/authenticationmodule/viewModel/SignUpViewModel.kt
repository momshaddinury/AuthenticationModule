package com.brainstation23.authenticationmodule.viewModel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.brainstation23.authenticationmodule.data.SignUpRequest
import com.brainstation23.authenticationmodule.data.SignUpResponse
import com.brainstation23.authenticationmodule.service.APIService
import kotlinx.coroutines.launch

class SignUpViewModel : ViewModel() {
    var state: SignUpState by mutableStateOf(SignUpState())

    private val apiService = APIService.getInstance()

    var email: String by mutableStateOf("")

    fun signUp(
        firstName: String,
        lastName: String,
        email: String,
        password: String,
    ) {
        viewModelScope.launch {
            try {
                state = state.copyWith(isLoading = true)

                SignUpRequest(
                    firstName = firstName,
                    lastName = lastName,
                    email = email,
                    password = password
                ).also {
                    this@SignUpViewModel.email = email
                    state = state.copyWith(response = apiService.signUp(it))
                }

            } catch (e: Exception) {
                e.printStackTrace()
                state = state.copyWith(isLoading = false, message = e.message.toString())
            } finally {
                state = state.copyWith(isLoading = false)
            }
        }
    }
}

class SignUpState {
    var isLoading: Boolean by mutableStateOf(false)
    var response: SignUpResponse? by mutableStateOf<SignUpResponse?>(null)
    var message: String by mutableStateOf("")

    fun copyWith(
        isLoading: Boolean = this.isLoading,
        response: SignUpResponse? = this.response,
        message: String = this.message
    ): SignUpState {
        return SignUpState().also {
            it.isLoading = isLoading
            it.response = response
            it.message = message
        }
    }
}