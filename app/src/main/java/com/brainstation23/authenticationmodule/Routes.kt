package com.brainstation23.authenticationmodule

import okhttp3.Route

sealed class Routes(val route: String) {
    object  Login: Routes("Login")
    object  ForgotPassword: Routes("ForgotPassword")
    object  SignUp: Routes("SignUp")
    object  Verification: Routes("Verification/{email}")
    object  ResetPassword: Routes("ResetPassword")
    object  Home: Routes("Home")
}
