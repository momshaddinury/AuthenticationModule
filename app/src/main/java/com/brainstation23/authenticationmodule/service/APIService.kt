package com.brainstation23.authenticationmodule.service

import com.brainstation23.authenticationmodule.data.LoginRequest
import com.brainstation23.authenticationmodule.data.LoginResponse
import com.brainstation23.authenticationmodule.data.SignUpRequest
import com.brainstation23.authenticationmodule.data.SignUpResponse
import com.brainstation23.authenticationmodule.data.VerificationRequest
import com.brainstation23.authenticationmodule.data.VerificationResponse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.http.Body
import retrofit2.http.POST


const val BASE_URL = "https://auth-module.sense-23.com/api/v1/"

interface APIService {
    @POST("auth/signUp")
    suspend fun signUp(@Body signUpRequest: SignUpRequest): SignUpResponse

    @POST("auth/verifyOtp")
    suspend fun verifyOtp(@Body verificationRequest: VerificationRequest): VerificationResponse

    @POST("auth/login")
    suspend fun login(@Body loginRequest: LoginRequest): LoginResponse

    companion object {
        private var apiService: APIService? = null
        fun getInstance(): APIService {
            if (apiService == null) {
                apiService = retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(retrofit2.converter.gson.GsonConverterFactory.create())
                    .client(logger())
                    .build()
                    .create(APIService::class.java)
            }
            return apiService!!
        }

        private fun logger(): OkHttpClient {
            val logging = HttpLoggingInterceptor()
            logging.setLevel(HttpLoggingInterceptor.Level.BODY)
            return OkHttpClient.Builder()
                .addInterceptor(logging)
                .build()
        }
    }
}