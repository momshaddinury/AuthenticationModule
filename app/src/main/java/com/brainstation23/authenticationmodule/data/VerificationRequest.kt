package com.brainstation23.authenticationmodule.data

data class VerificationRequest(
    var email: String,
    var otp: String,
)
