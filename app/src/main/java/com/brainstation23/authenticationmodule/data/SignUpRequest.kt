package com.brainstation23.authenticationmodule.data

import com.google.gson.annotations.SerializedName

data class SignUpRequest(
    @SerializedName("firstname")
    var firstName: String,
    @SerializedName("lastname")
    var lastName: String,
    var email: String,
    var password: String,
)
