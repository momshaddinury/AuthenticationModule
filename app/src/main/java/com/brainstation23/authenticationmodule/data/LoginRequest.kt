package com.brainstation23.authenticationmodule.data

import com.google.gson.annotations.SerializedName

data class LoginRequest(
    var email: String,
    var password: String,
    @SerializedName("OS")
    var os: String,
    var model: String,
    @SerializedName("FCMToken")
    var fcmToken: String,
)
