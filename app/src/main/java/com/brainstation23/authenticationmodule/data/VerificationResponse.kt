package com.brainstation23.authenticationmodule.data

data class VerificationResponse(
    var message: String,
)
