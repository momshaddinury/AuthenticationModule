package com.brainstation23.authenticationmodule.data

data class LoginResponse(
    var message: String,
    var token: String,
    var user: UserResponse,
)



