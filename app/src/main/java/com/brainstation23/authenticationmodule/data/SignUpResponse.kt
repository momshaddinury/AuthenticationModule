package com.brainstation23.authenticationmodule.data

data class SignUpResponse(
    var message: String,
)
