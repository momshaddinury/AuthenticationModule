package com.brainstation23.authenticationmodule.data

import com.google.gson.annotations.SerializedName

data class UserResponse(
    @SerializedName("firstname")
    var firstName: String,
    @SerializedName("lastname")
    var lastName: String,
    var email: String,
    var role: String,
)
