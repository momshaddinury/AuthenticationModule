package com.brainstation23.authenticationmodule.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

@Composable
fun FilledButton(
    text: String,
    isLoading: Boolean = false,
    onClick: () -> Unit,
    shape: RoundedCornerShape = RoundedCornerShape(50.dp),
    modifier: Modifier = Modifier
        .fillMaxWidth()
        .height(50.dp)
) {
    Button(
        onClick = onClick,
        shape = shape,
        modifier = modifier
    ) {
        if (isLoading) {
            CircularProgressIndicator(
                color = Color.White,
                modifier = Modifier.size(30.dp)
            )
        } else {
            Text(text = text)
        }
    }
}